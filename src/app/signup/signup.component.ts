import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { CommonService } from '../common.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {
  signupform :any=FormGroup;
  id:any=4;
  constructor(private fb:FormBuilder,private router:Router,private commServ:CommonService) { 
    
  }

  ngOnInit(): void {
    this.signupform=this.fb.group({
      name:['',Validators.required],
      email:['',Validators.required],
      password:['',Validators.required],
    })
  }
signupSubmit(data:any){
  console.log(data);
  let dataToPass={
    name:data.name,
    email:data.email,
    password:data.password,
    id:this.id++
  }
  this.commServ.addUser(dataToPass).subscribe((data:any)=>{
    console.log(data);
  });
}

}
