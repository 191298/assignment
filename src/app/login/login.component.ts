import { Component, OnInit } from '@angular/core';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { CommonService } from '../common.service';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginform :any =FormGroup;
  users:any=[];
  constructor(private fb:FormBuilder,private router:Router,private commserv:CommonService) {
  }
  ngOnInit(): void {
    this.loginform=this.fb.group({
      email:["",Validators.required],
      password:["",Validators.required]
    })
    this.commserv.getUser().subscribe((data:any)=>{
    this.users=data;
    })
  }
 
  loginSubmit(data:any){
if(data.email){
  this.users.forEach((item:any) => {
    if(item.email===data.email&& item.password===data.password)
 {
   localStorage.setItem("isLoggedIn","true");
   console.log("valid");
   this.router.navigate(['video']);

   //this.router.navigate(['login']);
 }  else{
  console.log("invalid");
  this.router.navigate(['video']);
 } 
 
  });
}
  }
}