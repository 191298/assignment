import { Pipe, PipeTransform } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';

@Pipe({
  name: 'sanitizer'
})
export class YoutubePipe implements PipeTransform {
constructor(private dom:DomSanitizer){}
  transform(value: string) {
    //console.log(value);
   // value=value+'';
   return this.dom.bypassSecurityTrustResourceUrl(value);

  }

}
