import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { VideoRoutingModule } from './video-routing.module';
import { VideoComponent } from './video.component';
import {MatToolbarModule} from '@angular/material/toolbar';
import { YoutubePipe } from '../youtube.pipe';
@NgModule({
  declarations: [
    VideoComponent,YoutubePipe , 
  ],
  imports: [
    CommonModule,
    VideoRoutingModule,
    MatToolbarModule,
    
  ]
})
export class VideoModule { }
