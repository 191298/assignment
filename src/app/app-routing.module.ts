import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './auth.guard';
import { LoginComponent } from './login/login.component';
import { VideoComponent } from './video/video.component';
const routes: Routes = [
{ path: 'login', loadChildren: () => import('./login/login.module').then(m => m.LoginModule) }, 
{ path: 'signup', loadChildren: () => import('./signup/signup.module').then(m => m.SignupModule) },
{ path: 'video', loadChildren: () => import('./video/video.module').then(m => m.VideoModule) },
{path:'**' ,component:LoginComponent},
{path:'video',component:VideoComponent,canActivate:[AuthGuard]},{path:'',redirectTo:'login',pathMatch:'full'}];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
